package ru.bk.klim9.newsfeed.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewsItem {

    @SerializedName("NewsItem")
    List<News> newsList;

    public List<News> getNewsList() {
        return newsList;
    }
}



