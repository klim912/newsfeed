package ru.bk.klim9.newsfeed.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import ru.bk.klim9.newsfeed.R;
import ru.bk.klim9.newsfeed.adapter.DividerItemDecoration;
import ru.bk.klim9.newsfeed.adapter.RecyclerClickListener;
import ru.bk.klim9.newsfeed.adapter.RecyclerViewAdapter;
import ru.bk.klim9.newsfeed.model.News;

public class ListFragment extends Fragment implements View.OnClickListener{

    final static String LOG_TAG = "myLog";

    RecyclerView recyclerView;
    List<News> mNewsList;

    Button updateButton;

    public static ListFragment newInstance(ArrayList<News> arrayList) {
        ListFragment listFragment = new ListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList("list", arrayList);
        listFragment.setArguments(args);
        return listFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mNewsList = new ArrayList<>();
        mNewsList = getArguments().getParcelableArrayList("list");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list,
                container, false);

        updateButton = (Button) view.findViewById(R.id.button);
        updateButton.setOnClickListener(this);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        final RecyclerViewAdapter adapter = new RecyclerViewAdapter(mNewsList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        RecyclerView.ItemDecoration itemDecoration =
                new DividerItemDecoration(10);
        recyclerView.addItemDecoration(itemDecoration);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerClickListener(getActivity()) {
            @Override
            public void onItemClick(RecyclerView recyclerView, View itemView,
                                    int position) {

                ListFragmentListener listener = (ListFragmentListener) getActivity();
                listener.itemClick(position);
                Log.d(LOG_TAG, String.valueOf(position));

            }
        });
        recyclerView.setHasFixedSize(true);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(itemAnimator);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button:
                ListFragmentListener listener = (ListFragmentListener) getActivity();
                listener.buttonClick();
                break;
        }

    }

    public interface ListFragmentListener {
        void buttonClick();
        void itemClick(int position);
    }





}
