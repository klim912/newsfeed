package ru.bk.klim9.newsfeed;

import android.app.FragmentTransaction;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;
import ru.bk.klim9.newsfeed.fragments.ListFragment;
import ru.bk.klim9.newsfeed.fragments.WebViewFragment;
import ru.bk.klim9.newsfeed.model.News;
import ru.bk.klim9.newsfeed.presenter.MainPresenter;

public class MainActivity extends AppCompatActivity implements MainPresenter.View,
        ListFragment.ListFragmentListener {

    final static String LOG_TAG = "myLog";

    private MainPresenter mainPresenter;
    private FragmentTransaction fragmentTransaction;
    private ListFragment listFragment;
    private WebViewFragment webViewFragment;
    // private ArrayList<News> mNewsList;

    TextView badAnswerText;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        mainPresenter = new MainPresenter(this);
        badAnswerText = (TextView) findViewById(R.id.textView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        // mNewsList = new ArrayList<News>();
        mainPresenter.createNewsList();

        /*
        if (savedInstanceState == null){
            mainPresenter.createNewsList();
            progressBar.setVisibility(View.VISIBLE);
        }else {
            mNewsList = savedInstanceState.getParcelableArrayList("mNewsList");
            mainPresenter.restoreInstanceState(mNewsList);
            for (int i = 0; i < mNewsList.size(); i++){
                Log.d(LOG_TAG, "restoreInstanceState = " + i + " " + mNewsList.get(i).getHeadLine());
                Log.d(LOG_TAG, "restoreInstanceState = " + i + " " + mNewsList.get(i).getWebURL());
            }

        }
        */


    }

    @Override
    public void updateListFragment(ArrayList<News> newsList) {

        listFragment = ListFragment.newInstance(newsList);
        listFragment.setRetainInstance(true);
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, listFragment);
        fragmentTransaction.commit();
        progressBar.setVisibility(View.GONE);
        // mNewsList = newsList;

    }

    @Override
    public void badAnswer() {
        badAnswerText.setText("Server returned an error response");
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void buttonClick() {
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.remove(listFragment);
        fragmentTransaction.commit();
        progressBar.setVisibility(View.VISIBLE);
        mainPresenter.createNewsList();

    }

    @Override
    public void itemClick(int position) {
        mainPresenter.itemClick(position);
    }

    @Override
    public void startWebViewFragment(String webUrl) {
        webViewFragment = WebViewFragment.newInstance(webUrl);
        webViewFragment.setRetainInstance(true);
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, webViewFragment);
        fragmentTransaction.commit();

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && this.webViewFragment != null && this.webViewFragment.canGoBack()) {
            this.webViewFragment.goBack();
            return true;
        }
        if (keyCode == KeyEvent.KEYCODE_BACK && this.webViewFragment.isVisible() && !this.webViewFragment.canGoBack()){
            fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, listFragment);
            fragmentTransaction.commit();
        }
        if (keyCode == KeyEvent.KEYCODE_BACK && this.listFragment.isVisible()){
            return super.onKeyDown(keyCode, event);
        } else return true;

    }

    /*
    @Override
    protected  Parcelable onSaveInstanceState()
    {
        Bundle bundle = new Bundle();
        bundle.putParcelable("superState", super.onSaveInstanceState());
        bundle.putParcelableArrayList("mNewsList", this.mNewsList); // ... save stuff
        return bundle;
    }

    @Override
    protected  void onRestoreInstanceState(Parcelable state)
    {
        if (state instanceof Bundle) // implicit null check
        {
            Bundle bundle = (Bundle) state;
            this.mNewsList = bundle.getParcelableArrayList("mNewsList"); // ... load stuff
            state = bundle.getParcelable("superState");
        }
        super.onRestoreInstanceState((Bundle) state);
    }
    */
}

