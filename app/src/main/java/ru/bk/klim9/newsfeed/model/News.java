package ru.bk.klim9.newsfeed.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class News implements Parcelable{

    @SerializedName("HeadLine")
    public final String headLine;

    @SerializedName("WebURL")
    public final String webURL;

    public News(String headLine, String webURL) {
        this.headLine = headLine;
        this.webURL = webURL;
    }

    protected News(Parcel in) {
        String[] data = new String[2];
        in.readStringArray(data);
        headLine = data[0];
        webURL = data[1];

    }

    public static final Creator<News> CREATOR = new Creator<News>() {
        @Override
        public News createFromParcel(Parcel in) {
            return new News(in);
        }

        @Override
        public News[] newArray(int size) {
            return new News[size];
        }
    };


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeStringArray(new String[]{headLine, webURL});
    }

    public String getHeadLine() {
        return headLine;
    }

    public String getWebURL() {
        return webURL;
    }
}
