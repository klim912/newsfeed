package ru.bk.klim9.newsfeed.presenter;

import retrofit.Call;
import retrofit.http.GET;
import ru.bk.klim9.newsfeed.model.NewsItem;

public interface Timesofindia {
    @GET("/feeds/newsdefaultfeeds.cms?feedtype=sjson")
    Call<NewsItem> callNewsItem();
}
