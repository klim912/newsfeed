package ru.bk.klim9.newsfeed.presenter;

import android.os.Parcelable;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ru.bk.klim9.newsfeed.model.News;
import ru.bk.klim9.newsfeed.model.NewsItem;

import com.squareup.okhttp.ResponseBody;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class MainPresenter {

    final static String LOG_TAG = "myLog";

    private ArrayList<News> mNewsList;
    private View view;

    public MainPresenter(View view){
        this.view = view;
        mNewsList = new ArrayList<News>();
    }

    public void createNewsList() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SimpleService.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Timesofindia timesofindia = retrofit.create(Timesofindia.class);

        Call<NewsItem> call = timesofindia.callNewsItem();

        call.enqueue(new Callback<NewsItem>() {
            @Override
            public void onResponse(Response<NewsItem> response, Retrofit retrofit) {
                if (response.isSuccess()) {

                    NewsItem newsItem = response.body();

                    for (int i = 0; i < newsItem.getNewsList().size(); i++){
                        Log.d(LOG_TAG, "callNewsItem = " + i + " " + newsItem.getNewsList().get(i).getHeadLine());
                        Log.d(LOG_TAG, "callNewsItem = " + i + " " + newsItem.getNewsList().get(i).getWebURL());
                    }

                    mNewsList = (ArrayList<News>) newsItem.getNewsList();
                    view.updateListFragment(mNewsList);

                } else {

                    int statusCode = response.code();
                    ResponseBody errorBody = response.errorBody();

                    view.badAnswer();

                }
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });

    }

    public void itemClick(int position) {
        String webUrl = mNewsList.get(position).getWebURL();
        view.startWebViewFragment(webUrl);
    }

    public void restoreInstanceState(ArrayList<News> newsList){
        mNewsList = newsList;

    }

    public interface View {
        void updateListFragment(ArrayList<News> newsList);
        void badAnswer();
        void startWebViewFragment(String webUrl);

    }
}
