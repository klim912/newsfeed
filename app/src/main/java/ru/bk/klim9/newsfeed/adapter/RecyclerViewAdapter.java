package ru.bk.klim9.newsfeed.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ru.bk.klim9.newsfeed.R;
import ru.bk.klim9.newsfeed.model.News;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    private List<News> mNewsList;

    public RecyclerViewAdapter(List<News> newsList) {
        this.mNewsList = newsList;
    }

    @Override
    public RecyclerViewAdapter.MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recyclerview_item, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder viewHolder, int i) {
        News news = mNewsList.get(i);

        viewHolder.mHeadLineTextView.setText(news.getHeadLine());

    }

    @Override
    public int getItemCount() {
        return mNewsList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView mHeadLineTextView;

        public MyViewHolder(View itemView) {
            super(itemView);
            mHeadLineTextView = (TextView) itemView.findViewById(R.id.text_head_line);
        }
    }
}
